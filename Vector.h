/*
* Created by Rajesh Moturu on 20.08.2022
*/

#pragma once

#include <iostream>


namespace vec
{
template<class T>
class Vector
{
public:
  Vector();

  explicit Vector(std::size_t n);  // simple constructor with n elements

  Vector(std::size_t n, T value);  // constructor with n elements and filled with value

  Vector(std::initializer_list<T> lst);  // constructor with initializer list

  Vector(const Vector<T>& v);  // copy constructor
  Vector<T>& operator=(const Vector<T>& v);  // copy assignment operator

  Vector(Vector<T>&& v);  // move constructor
  Vector<T>& operator=(Vector<T>&& v);  // move assignment operator

  ~Vector();  // destructor

  T& operator[](std::size_t idx) const;

  [[nodiscard]] std::size_t size() const;

  void resize(std::size_t new_size);

  void push_back(const T& value);

  void reserve(std::size_t n);

  void print(const std::string& vec_name) const;

private:
  std::size_t m_size;
  T* m_elem;
  std::size_t m_capacity;
};

template<typename T>
Vector<T>::Vector() : m_size(0), m_elem(nullptr), m_capacity(0)
{ }

template<typename T>
Vector<T>::Vector(std::size_t n) : m_size(n), m_elem(new T[n])
{

}

template<typename T>
Vector<T>::Vector(std::size_t n, T value) : m_size(n), m_elem(new T[n]), m_capacity(n)
{
  for (std::size_t i = 0; i < n; ++i) {
    m_elem[i] = value;
  }
}

template<typename T>
Vector<T>::Vector(std::initializer_list<T> lst) : m_size(lst.size()), m_elem(new T[lst.size()]), m_capacity(lst.size())
{
  std::size_t i = 0;
  for (auto it = lst.begin(); it != lst.end(); ++it, ++i) {
    m_elem[i] = *it;
  }
}

template<typename T>
Vector<T>::Vector(const Vector<T>& v) : m_size(v.size()), m_elem(new T[v.size()]), m_capacity(v.size())
{
  for (std::size_t i = 0; i < m_size; ++i) {
    m_elem[i] = v[i];
  }
}

template<typename T>
Vector<T>& Vector<T>::operator=(const Vector<T>& v)
{
  if (this == &v) {
    return *this;
  }

  if (m_capacity >= v.m_size) {  // if we have enough space already, just copy and return
    for (std::size_t i = 0; i < v.m_size; ++i) {
      m_elem[i] = v[i];
    }
    return *this;
  }

  m_size = v.m_size;
  T* tmp = new T[v.m_size];
  for (std::size_t i = 0; i < v.m_size; ++i) {
    tmp[i] = v[i];
  }
  delete[] m_elem;  // delete previously allocated memory to avoid memory leak
  m_elem = tmp;

  return *this;
}

template<typename T>
Vector<T>::Vector(Vector<T>&& v) : m_size(v.m_size), m_elem(new T[v.size()]), m_capacity(v.m_size)
{
  for (std::size_t i = 0; i < v.m_size; ++i) {
    m_elem[i] = v[i];
  }
  v.m_size = 0;
  v.m_elem = nullptr;
  v.m_capacity = 0;
}

template<typename T>
Vector<T>& Vector<T>::operator=(Vector<T>&& v)
{
  if (this == &v) {
    return *this;
  }

  if (m_capacity >= v.m_size) {  // if we have enough space already, just copy and return
    for (std::size_t i = 0; i < v.m_size; ++i) {
      m_elem[i] = v[i];
    }
    v.m_size = 0;
    v.m_capacity = 0;
    v.m_elem = nullptr;

    return *this;
  }

  delete[] m_elem;
  m_size = v.m_size;
  m_elem = v.m_elem;  // use already in place copy assignment operator
  v.m_size = 0;
  v.m_elem = nullptr;

  return *this;
}

template<typename T>
Vector<T>::~Vector()
{
  delete[] m_elem;
}

template<typename T>
T& Vector<T>::operator[](const std::size_t idx) const
{
  return m_elem[idx];
}

template<typename T>
std::size_t Vector<T>::size() const
{
  return m_size;
}

template<typename T>
void Vector<T>::print(const std::string& vec_name) const
{
  std::string separator;
  std::cout << "\nPrinting vector: " << vec_name << "\n";
  for (std::size_t i = 0; i < m_size; ++i) {
    std::cout << separator << m_elem[i];
    separator = ", ";
  }
  std::cout << "\n";
}

template<typename T>
void Vector<T>::reserve(const std::size_t new_capacity)
{
  if (new_capacity <= m_capacity) {
    return;
  }
  T* temp = new T[new_capacity];
  for (std::size_t i = 0; i < m_size; ++i) {
    temp[i] = m_elem[i];
  }
  delete[] m_elem;
  m_elem = temp;
  m_capacity = new_capacity;
}

template<typename T>
void Vector<T>::resize(const std::size_t new_size)
{
  reserve(new_size);
  m_size = new_size;
}

template<typename T>
void Vector<T>::push_back(const T& value)
{
  if (m_capacity == 0) {
    reserve(8);
  }
  if (m_size == m_capacity) {  // double the storage if the vector is full to avoid multiple reserves
    reserve(m_capacity * 2);
  }
  m_elem[m_size] = value;
  ++m_size;
}

}  // vec namespace
