#include <iostream>

#include "Vector.h"


using namespace vec;

int main()
{
  // Vector with space for 5 doubles.
  const Vector<double> double_vec(5);
  double_vec.print("double_vec");

  // Vector with space for 4 floats initialized to 0.2f
  const Vector<float> float_vec(4, 0.2f);
  float_vec.print("float_vec");

  // Vector of 3 ints by initializer lists
  const Vector<int> int_vec{1, 2, 3};
  int_vec.print("int_vec");

  // copy constructor and assignment
  Vector<std::string> str_vec1(4, "xyz");
  Vector<std::string> str_vec2(str_vec1);
  str_vec2.print("str_vec2");
  Vector<std::string> str_vec3;
  str_vec3 = str_vec1;
  str_vec3.print("str_vec3");

  // move constructor and move assignment
  Vector<double> d_vec1(4, 0.012312);
  Vector<double> d_vec2(std::move(d_vec1));
  d_vec2.print("d_vec2");
  Vector<double> d_vec3;
  d_vec3 = d_vec2;
  d_vec3.print("d_vec3");

  // push back
  d_vec3.push_back(2.3434);
  d_vec3.print("d_vec3");

  // size
  std::cout << "Size: " << d_vec3.size() << "\n";

  return 0;
}
